/*
Bài 1: Tính lương nhân viên
Lương 1 ngày: 100.000
Số ngày làm
Công thức = lương 1 ngày * số ngày làm
*/

// Làm lần 2
function tinhLuongThang() {
  var luongNgay = document.getElementById("luong1Ngay").value * 1;
  var soNgay = document.getElementById("soNgayLam").value * 1;
  var tongLuong = 0;
  tongLuong = luongNgay * soNgay;
  document.getElementById("tongLuong").innerHTML = tongLuong.toLocaleString();
}
