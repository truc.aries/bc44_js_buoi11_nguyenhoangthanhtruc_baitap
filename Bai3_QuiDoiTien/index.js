/*
Bài 3 Qui đổi tiền
Qui đổi từ USD sang VND
*/
// Làm lần 2
function quiDoiTien() {
  var tienUsd = document.getElementById("tien-usd").value * 1;
  var tienVnd = document.getElementById("tien-vnd").value * 1;
  var soTienQuiDoi = tienUsd * tienVnd;
  document.getElementById("soTienQuiDoi").innerHTML = new Intl.NumberFormat(
    "vn-VN"
  ).format(soTienQuiDoi);
}
