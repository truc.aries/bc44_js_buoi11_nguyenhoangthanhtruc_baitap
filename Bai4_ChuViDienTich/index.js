/*
Bài 4 Tính chu vi diện tích hình chữ nhật
Diện tích = dài * rộng
Chu vi = (dài + rộng)*2
*/

// Làm lần 2
function tinhToan() {
  var chieuDai = document.getElementById("chieu-dai").value * 1;
  var chieuRong = document.getElementById("chieu-rong").value * 1;
  var chuVi = (chieuDai + chieuRong) * 2;
  var dienTich = chieuDai * chieuRong;
  document.getElementById("chuVi").innerHTML = chuVi;
  document.getElementById("dienTich").innerHTML = dienTich;
}
