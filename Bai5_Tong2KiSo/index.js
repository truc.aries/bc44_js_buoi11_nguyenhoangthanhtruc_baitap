/*
Bài 5 tính tổng 2 kí số
*/

function tinhTongKiSo() {
  var number = document.getElementById("txt-so").value * 1;
  var hangChuc = Math.floor(number / 10);
  var hangDonVi = number % 10;
  var tongKiSo = hangChuc + hangDonVi;
  document.getElementById("tongKiSo").innerHTML = tongKiSo;
}
