/*
Bài 2: Tính giá trị trung bình 5 số thực
Công thức = (Tổng 5 số) / 5
*/

// Làm lần 2
function tinhTrungBinh() {
  var soThu1 = document.getElementById("soThu1").value * 1;
  var soThu2 = document.getElementById("soThu2").value * 1;
  var soThu3 = document.getElementById("soThu3").value * 1;
  var soThu4 = document.getElementById("soThu4").value * 1;
  var soThu5 = document.getElementById("soThu5").value * 1;
  var tinhTrungBinh = (soThu1 + soThu2 + soThu3 + soThu4 + soThu5) / 5;
  document.getElementById("soTrungBinh").innerHTML =
    tinhTrungBinh.toLocaleString();
}
